import 'package:flutter/material.dart';
import 'plan_provider.dart  ';
import 'views/app.dart';


void main() {
  var planProvider = PlanProvider(child: MyPlanApp());
  runApp(planProvider);
}
